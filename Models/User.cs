﻿using System;
using System.Collections.Generic;

namespace WeightScanAPI.Models
{
    public partial class User
    {
        public User()
        {
            Login = new HashSet<Login>();
        }

        public int Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public string Role { get; set; }

        public ICollection<Login> Login { get; set; }
    }
}
