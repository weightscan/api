﻿using System;
using System.Collections.Generic;

namespace WeightScanAPI.Models
{
    public partial class Pesage
    {
        public int Id { get; set; }
        public int IdPersonne { get; set; }
        public double Poid { get; set; }
        public DateTime? DatePesage { get; set; }

        public Personne IdPersonneNavigation { get; set; }
    }
}
