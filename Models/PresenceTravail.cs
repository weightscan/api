﻿using System;
using System.Collections.Generic;

namespace WeightScanAPI.Models
{
    public partial class PresenceTravail
    {
        public int Id { get; set; }
        public int IdPersonne { get; set; }
        public DateTime DateCommence { get; set; }
        public DateTime DateFinie { get; set; }

        public Personne IdPersonneNavigation { get; set; }
    }
}
