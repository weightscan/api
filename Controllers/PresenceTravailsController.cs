﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WeightScanAPI.Models;

namespace WeightScanAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PresenceTravailsController : ControllerBase
    {
        private readonly WeightScanDBContext _context;

        public PresenceTravailsController(WeightScanDBContext context)
        {
            _context = context;
        }

        // GET: api/PresenceTravails
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PresenceTravail>>> GetPresenceTravail()
        {
            return await _context.PresenceTravail.ToListAsync();
        }

        // GET: api/PresenceTravails/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PresenceTravail>> GetPresenceTravail(int id)
        {
            var presenceTravail = await _context.PresenceTravail.FindAsync(id);

            if (presenceTravail == null)
            {
                return NotFound();
            }

            return presenceTravail;
        }

        // PUT: api/PresenceTravails/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPresenceTravail(int id, PresenceTravail presenceTravail)
        {
            if (id != presenceTravail.Id)
            {
                return BadRequest();
            }

            _context.Entry(presenceTravail).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PresenceTravailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PresenceTravails
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<PresenceTravail>> PostPresenceTravail(PresenceTravail presenceTravail)
        {
            _context.PresenceTravail.Add(presenceTravail);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPresenceTravail", new { id = presenceTravail.Id }, presenceTravail);
        }

        // DELETE: api/PresenceTravails/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<PresenceTravail>> DeletePresenceTravail(int id)
        {
            var presenceTravail = await _context.PresenceTravail.FindAsync(id);
            if (presenceTravail == null)
            {
                return NotFound();
            }

            _context.PresenceTravail.Remove(presenceTravail);
            await _context.SaveChangesAsync();

            return presenceTravail;
        }

        private bool PresenceTravailExists(int id)
        {
            return _context.PresenceTravail.Any(e => e.Id == id);
        }
        [HttpGet("matricule/{matricule}")]
        public IEnumerable<PresenceTravail> GetPointagePersonne([FromRoute] string matricule)
        {
            return _context.PresenceTravail.Where(p => p.IdPersonneNavigation.Matricule == matricule).Include(l => l.IdPersonneNavigation)
           .ToList();
        }

        [HttpGet("today")]
        public IEnumerable<PresenceTravail> GetPesageToday()
        {
            return _context.PresenceTravail.Where(p => p.DateCommence.Date == DateTime.Now.Date).Include(l => l.IdPersonneNavigation)
            .ToList();
        }
        [HttpGet("Personne/{id}")]
        public IEnumerable<PresenceTravail> GetPesagePersonne([FromRoute] int id)
        {
            return _context.PresenceTravail.Where(p => p.IdPersonne == id).Include(l => l.IdPersonneNavigation)
            .ToList();
        }
        [HttpGet("le/{date}")]
        public async Task<ActionResult<IEnumerable<PresenceTravail>>> GetPesageDate([FromRoute] DateTime date)
        {
            return await _context.PresenceTravail.Where(d => d.DateCommence.Date == date.Date).Include(l => l.IdPersonneNavigation).ToListAsync();
        }
        [HttpGet("du/{du}/au/{au}")]
        public async Task<ActionResult<IEnumerable<PresenceTravail>>> GetPesagePeriod([FromRoute] DateTime du, [FromRoute] DateTime au)
        {
            return await _context.PresenceTravail.Where(d => d.DateCommence.Date >= du.Date && d.DateCommence.Date <= au.Date).Include(l => l.IdPersonneNavigation).ToListAsync();
        }
        [HttpGet("matricule/{matricule}/du/{du}/au/{au}")]
        public async Task<ActionResult<IEnumerable<PresenceTravail>>> GetPesagePeriodPersonne([FromRoute] string matricule, [FromRoute] DateTime du, [FromRoute] DateTime au)
        {
            return await _context.PresenceTravail.Where(d => d.DateCommence.Date >= du.Date && d.DateCommence.Date <= au.Date && d.IdPersonneNavigation.
            Matricule == matricule).Include(l => l.IdPersonneNavigation).ToListAsync();
        }
        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<PresenceTravail>>> GetAllPointages()
        {
            return await _context.PresenceTravail
            .Include(l => l.IdPersonneNavigation)
            .ToListAsync();
        }
        [HttpGet("Count")]
        public async Task<ActionResult<IEnumerable<PresenceTravail>>> GetCountPoint()
        {
            return await _context.PresenceTravail
            .Include(l => l.IdPersonneNavigation)
            .ToListAsync();
        }

        [HttpGet("totalPresence/{du}/{au}")]
        public IQueryable getTotalPresence(DateTime du, DateTime au)
        {
            var result = from pt in _context.PresenceTravail
                         where pt.DateCommence.Date >= du.Date && pt.DateCommence.Date <= au.Date
                         group pt by new
                         {
                             pt.IdPersonne,
                             pt.IdPersonneNavigation.Id,
                             pt.IdPersonneNavigation.Matricule,
                             pt.IdPersonneNavigation.Nom,
                             pt.IdPersonneNavigation.Prenom
                         }
                         into ppt
                         select new
                         {
                             IdPersonneNavigation = ppt.Key,
                             Total = ppt.Count()

                         };
            return result;
        }
    }
}