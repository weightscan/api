﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WeightScanAPI.Models;

namespace WeightScanAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PesagesController : ControllerBase
    {
        private readonly WeightScanDBContext _context;

        public PesagesController(WeightScanDBContext context)
        {
            _context = context;
        }

        // GET: api/Pesages
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Pesage>>> GetPesage()
        {
            return await _context.Pesage.ToListAsync();
        }


        [HttpGet("time/{id}")]
        public double get(int _id)
        {
            //return _context.Pesage.Where(p => p.IdPersonne == 1886).Count();
            Pesage pes;
            pes = _context.Pesage.Where(p => p.IdPersonne == _id)
                .OrderByDescending(p => p.DatePesage)
                .Take(1).FirstOrDefault();
            TimeSpan ts;
            if (pes != null)
            {
                ts = DateTime.Now - pes.DatePesage.Value;
            }
            else
            {
                ts = DateTime.Now - new DateTime(2020, 01, 01);
            }
            return ts.TotalMinutes;
        }

        // GET: api/Pesages/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Pesage>> GetPesage(int id)
        {
            var pesage = await _context.Pesage.FindAsync(id);

            if (pesage == null)
            {
                return NotFound();
            }

            return pesage;
        }

        // PUT: api/Pesages/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPesage(int id, Pesage pesage)
        {
            if (id != pesage.Id)
            {
                return BadRequest();
            }

            _context.Entry(pesage).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PesageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Pesages
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Pesage>> PostPesage(Pesage pesage)
        {
            PresenceTravail presence = new PresenceTravail();
            pesage.DatePesage = DateTime.Now;
            int _id = pesage.IdPersonne;
            int p = _context.Pesage.Where(pp => pp.DatePesage.Value.Date == DateTime.Now.Date && pp.IdPersonne == _id).Count();
            if (p == 0)
            {
                presence.IdPersonne = _id;
                presence.DateCommence = DateTime.Now;
                presence.DateFinie = DateTime.Now;
                _context.PresenceTravail.Add(presence);
            }
            Pesage pes;
            pes = _context.Pesage.Where(pp => pp.IdPersonne == _id)
                .OrderByDescending(pp => pp.DatePesage)
                .Take(1).FirstOrDefault();
            TimeSpan ts;
            if (pes != null)
            {
                ts = DateTime.Now - pes.DatePesage.Value;
            }
            else
            {
                ts = DateTime.Now - new DateTime(2020, 01, 01);
            }
            int count = _context.Pesage.Where(pp => pp.IdPersonne == _id).Count();

            if (count == 0)
            {
                _context.Pesage.Add(pesage);
            }
            else if (ts.TotalMinutes > 30)
            {
                _context.Pesage.Add(pesage);
            }
            //_context.Pesage.Add(pesage);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPesage", new { id = pesage.Id }, pesage);

        }

        // DELETE: api/Pesages/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Pesage>> DeletePesage(int id)
        {
            var pesage = await _context.Pesage.FindAsync(id);
            if (pesage == null)
            {
                return NotFound();
            }

            _context.Pesage.Remove(pesage);
            await _context.SaveChangesAsync();

            return pesage;
        }

        private bool PesageExists(int id)
        {
            return _context.Pesage.Any(e => e.Id == id);
        }


        //actions 
        [HttpGet("today")]
        public IEnumerable<Pesage> GetPesageToday()
        {
            return _context.Pesage.Where(p => p.DatePesage.Value.Date == DateTime.Now.Date).Include(l => l.IdPersonneNavigation)
            .ToList();
        }
        [HttpGet("get")]
        public IQueryable GetPesageAndTotalPesage()
        {
            //var pesages=_context.Pesage.Select(p => new
            //{
            //    matricule = p.IdPersonneNavigation.Matricule,
            //    nom = p.IdPersonneNavigation.Nom,
            //    prenom = p.IdPersonneNavigation.Prenom,
            //    Total = p.Sum(s=>s.Poid)
            //}).GroupBy(p => new {
            //    p.IdPersonneNavigation.Matricule,
            //    p.IdPersonneNavigation.Nom,
            //    p.IdPersonneNavigation.Prenom
            //})

            var result = from p in _context.Pesage
                         join ps in _context.Personne on p.IdPersonne equals ps.Id
                         where p.DatePesage.Value.Date == DateTime.Now.Date
                         group p by new
                         {
                             p.IdPersonne,
                             p.IdPersonneNavigation.Matricule,
                             p.IdPersonneNavigation.Nom,
                             p.IdPersonneNavigation.Prenom
                         } into pp
                         select new
                         {
                             Matricule = pp.Key.Matricule,
                             Nom = pp.Key.Nom,
                             Prenom = pp.Key.Prenom,
                             Total = pp.Sum(s => s.Poid),
                             prixTotal = 0
                         };
            return result;
            //return _context.Pesage.Where(p => p.DatePesage.Value.Date == DateTime.Now.Date).Include(l => l.IdPersonneNavigation)
            //.ToList();

        }
        [HttpGet("lastPesgae/{id}")]
        public bool GetLastPesage(int id)
        {
            //if (_context.Pesage.Where(p => p.IdPersonne==id && (p.DatePesage.Value.Minute - DateTime.Now.Minute>30)){

            //}

            return true;
        }
        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<Pesage>>> GetAllPesage()
        {
            return await _context.Pesage
            .Include(l => l.IdPersonneNavigation)
            .ToListAsync();
        }

        [HttpGet("AllPesage/{mat}")]
        public async Task<ActionResult<IEnumerable<Pesage>>> GetAllPesagePersonne([FromRoute] string mat)
        {
            return await _context.Pesage.Where(p => p.IdPersonneNavigation.Matricule == mat && p.DatePesage.Value.Date == DateTime.Now.Date)
            .Include(l => l.IdPersonneNavigation)
            .ToListAsync();
        }

        [HttpGet("le/{date}")]
        public IQueryable GetPesageDate([FromRoute] DateTime date)
        {
            var result = from p in _context.Pesage
                         join ps in _context.Personne on p.IdPersonne equals ps.Id
                         where p.DatePesage.Value.Date == date
                         group p by new
                         {
                             p.IdPersonne,
                             p.IdPersonneNavigation.Matricule,
                             p.IdPersonneNavigation.Nom,
                             p.IdPersonneNavigation.Id,
                             p.IdPersonneNavigation.Prenom
                         } into pp
                         select new
                         {
                             Matricule = pp.Key.Matricule,
                             Nom = pp.Key.Nom,
                             Prenom = pp.Key.Prenom,
                             Total = pp.Sum(s => s.Poid),
                             PrixTotal = 0
                         };
            return result;
            //return await _context.Pesage.Where(d => d.DatePesage.Value.Date == date.Date).Include(l => l.IdPersonneNavigation).ToListAsync();
        }

        [HttpGet("du/{du}/au/{au}")]
        public IQueryable GetPesagePeriod([FromRoute] DateTime du, [FromRoute] DateTime au)
        {
            //return await _context.Pesage.Where(d => d.DatePesage.Value.Date >= du.Date && d.DatePesage.Value.Date <= au.Date).Include(l => l.IdPersonneNavigation).ToListAsync();

            var result = from p in _context.Pesage
                         join ps in _context.Personne on p.IdPersonne equals ps.Id
                         where p.DatePesage.Value >= du && p.DatePesage.Value <= au
                         group p by new
                         {
                             p.IdPersonne,
                             p.IdPersonneNavigation.Matricule,
                             p.IdPersonneNavigation.Nom,
                             p.IdPersonneNavigation.Id,
                             p.IdPersonneNavigation.Prenom
                         } into pp
                         select new
                         {
                             Matricule = pp.Key.Matricule,
                             Nom = pp.Key.Nom,
                             Prenom = pp.Key.Prenom,
                             Total = pp.Sum(s => s.Poid),

                         };
            return result;
        }

        [HttpGet("matricule/{mat}/du/{du}/au/{au}")]
        public IQueryable GetPesagePeriodPersonne([FromRoute] string mat, [FromRoute] DateTime du, [FromRoute] DateTime au)
        {
            return _context.Pesage.Where(d => d.IdPersonneNavigation.Matricule == mat && d.DatePesage.Value.Date >= du.Date && d.DatePesage.Value.Date <= au.Date);


        }

        [HttpGet("exportVerExcel/{mat}/{du}/{au}")]
        public IQueryable ExportExcel(string mat, DateTime du, DateTime au)
        {
            return _context.Pesage.Where(p => p.IdPersonneNavigation.Matricule == mat
           && p.DatePesage.Value.Date >= du.Date && p.DatePesage.Value.Date <= au.Date).Include(l => l.IdPersonneNavigation)
            .Select(p => new {
                Date = p.DatePesage.Value.Date,
                Heure = p.DatePesage.Value.TimeOfDay,
                Poide = p.Poid
            });

            //select new
            //{
            //    Date=p.DatePesage.Value.Date,
            //    Heure=p.DatePesage.Value.Hour,
            //    Poide=p.Poid
            //};


        }
        [HttpGet("exportAll/{du}/{au}")]
        public IQueryable ExportAllToExcel([FromRoute] DateTime du, [FromRoute] DateTime au)
        {

            //var result = _context.Pesage.Include(l => l.IdPersonneNavigation);
            //.ToList();
            var result = from p in _context.Pesage
                         join ps in _context.Personne on p.IdPersonne equals ps.Id
                         where p.DatePesage.Value >= du && p.DatePesage.Value <= au
                         group p by new
                         {
                             p.IdPersonne,
                             p.IdPersonneNavigation.Matricule,
                             p.IdPersonneNavigation.Nom,
                             p.IdPersonneNavigation.Id,
                             p.IdPersonneNavigation.Prenom
                         } into pp
                         select new
                         {
                             Matricule = pp.Key.Matricule,
                             Nom = pp.Key.Nom,
                             Prenom = pp.Key.Prenom,
                             Total = pp.Sum(s => s.Poid),
                             PrixTotal = 0
                         };
            return result;

        }
        [HttpGet("exportAll/{du}/{au}")]
        public IQueryable ExportAllToExcele([FromRoute] DateTime du, [FromRoute] DateTime au)
        {

            //var result = _context.Pesage.Include(l => l.IdPersonneNavigation);
            //.ToList();
            var result = from p in _context.Pesage
                         join ps in _context.Personne on p.IdPersonne equals ps.Id
                         where p.DatePesage.Value >= du && p.DatePesage.Value <= au
                         group p by new
                         {
                             p.IdPersonne,
                             p.IdPersonneNavigation.Matricule,
                             p.IdPersonneNavigation.Nom,
                             p.IdPersonneNavigation.Id,
                             p.IdPersonneNavigation.Prenom
                         } into pp
                         select new
                         {
                             Matricule = pp.Key.Matricule,
                             Nom = pp.Key.Nom,
                             Prenom = pp.Key.Prenom,
                             Total = pp.Sum(s => s.Poid),
                             PrixTotal = 0
                         };
            return result;

        }
        //alert 
        [HttpGet("alert")]
        public IQueryable alert()
        {

            var result = from ps in _context.Pesage
                         join pr in _context.Personne on ps.IdPersonne equals pr.Id
                         //where (DateTime.Now.Hour-ps.DatePesage.Value.Hour)>=1 && ps.DatePesage.Value==DateTime.Now.Date
                         group ps by new
                         {
                             ps.IdPersonne,
                             ps.IdPersonneNavigation.Matricule,
                             ps.IdPersonneNavigation.Nom,
                             ps.IdPersonneNavigation.Id,
                             ps.IdPersonneNavigation.Prenom,
                         } into pp
                         // where (DateTime.Now.Hour - datePesage.Value.Hour) >= 1 && datePesage.Value == DateTime.Now.Date
                         select new
                         {
                             datePesage = (from pes in _context.Pesage
                                           where pes.IdPersonne == pp.Key.IdPersonne

                                           orderby pes.DatePesage descending
                                           select pes.DatePesage).FirstOrDefault(),
                             idPersonneNavigation = pp.Key
                         };

            return result;
        }
        [HttpGet("getPrixTotal/{prix}/{du}/{au}")]
        public IQueryable getPrixTotal(int prix, DateTime du, DateTime au)
        {
            var result = from p in _context.Pesage
                         join ps in _context.Personne on p.IdPersonne equals ps.Id
                         where p.DatePesage.Value.Date >= du && p.DatePesage.Value.Date <= au
                         group p by new
                         {
                             p.IdPersonne,
                             p.IdPersonneNavigation.Matricule,
                             p.IdPersonneNavigation.Nom,
                             p.IdPersonneNavigation.Id,
                             p.IdPersonneNavigation.Prenom
                         } into pp
                         select new
                         {
                             Matricule = pp.Key.Matricule,
                             Nom = pp.Key.Nom,
                             Prenom = pp.Key.Prenom,
                             Total = pp.Sum(s => s.Poid),
                             PrixTotal = prix * pp.Sum(s => s.Poid)
                         };
            return result;
        }
    }
}